package com.example.android.miwok;

/**
 * Created by Muzammil on 8/17/2016.
 */
public class Word {
    String mdefaulttranslation;
    String miwokTranslation;
    int mImageResourceId = NO_IMAGE_PROVIDED;
    private int mMusicResourceId;
    private static final int NO_IMAGE_PROVIDED = -1;

    public Word(String mdefaulttranslation, String miwokTranslation, int mMusicResourceId){
        this.miwokTranslation = miwokTranslation;
        this.mdefaulttranslation = mdefaulttranslation;
        this.mMusicResourceId = mMusicResourceId;
    }

    /**
     *  @param mdefaulttranslation is the word in language that the user already familiar with
     * @param miwokTranslation is the word in miwok langugae
     * @param mImageResourceId is the drawable resource id for image asset
     * @param mMusicResourceId
     */

    public Word(String mdefaulttranslation, String miwokTranslation, int mImageResourceId, int mMusicResourceId) {
        this.mdefaulttranslation = mdefaulttranslation;
        this.miwokTranslation = miwokTranslation;
        this.mImageResourceId = mImageResourceId;
        this.mMusicResourceId = mMusicResourceId;
    }

    public String getMdefaulttranslation() {
        return mdefaulttranslation;
    }

    public String getMiwokTranslation() {
        return miwokTranslation;
    }

    public void setMdefaulttranslation(String mdefaulttranslation) {
        this.mdefaulttranslation = mdefaulttranslation;
    }

    public void setMiwokTranslation(String miwokTranslation){
        this.miwokTranslation = miwokTranslation;
    }

    public int getmImageResourceId() {
        return mImageResourceId;
    }

    public void setmImageResourceId(int mImageResourceId) {
        this.mImageResourceId = mImageResourceId;
    }

    public boolean hasImageResource(){
        return mImageResourceId != NO_IMAGE_PROVIDED;
    }

    public int getmMusicResourceId() {
        return mMusicResourceId;
    }

    public void setmMusicResourceId(int mMusicResourceId) {
        this.mMusicResourceId = mMusicResourceId;
    }
}
